﻿using UnityEngine;
using System.Collections;

public class SimpleBrick : MonoBehaviour
{
	private LevelManager levelManager;
    private Color whiteAlpha;
    private bool doAlpha;
    private float currentAlpha;
    private GameObject brickShadow;
	public static int breakableCount =0;

	//
	// Use this for initialization
	void Start ()
	{
		levelManager = GameObject.FindObjectOfType<LevelManager> ();

        whiteAlpha.r = 1f;
        whiteAlpha.g = 1f;
        whiteAlpha.b = 1f;
        whiteAlpha.a = 1f;
        doAlpha = false;
        currentAlpha = 1f;

        ++breakableCount;


       // GetComponentInChildren<ScreenManager>().UpdateScore(10);

	}
	
	// Update is called once per frame
	void Update ()
	{
        if (doAlpha) SetAlpha();
	}

	void OnCollisionExit2D (Collision2D collision)
	{
        AudioSource audio = GetComponent<AudioSource>();
        audio.volume =  LevelManager.soundLevel;
        audio.pitch = Random.Range(0.9f, 1.1f);
        audio.Play();
        //AudioSource.PlayClipAtPoint(boing, transform.position);
        HandleHit();	
	}

	void HandleHit()
	{
        //DestroyObject(gameObject);
       
        // Disable the Boxcollider2D component.
        GetComponent<BoxCollider2D>().enabled = false;
        doAlpha = true;

        if (LevelManager.currentlyP1) ScreenManager.P1Score += 1;
        if (!LevelManager.currentlyP1) ScreenManager.P2Score += 1;

        --breakableCount;
        if (breakableCount <= 0) levelManager.LoadNextLevel();
	}

    void SetAlpha()
    {
        whiteAlpha.a = currentAlpha;
        GetComponent<SpriteRenderer>().color = whiteAlpha;

        // Alpha for shadow childobject
        whiteAlpha.a = currentAlpha/2;
        transform.GetChild(0).GetComponent<SpriteRenderer>().color = whiteAlpha;

        currentAlpha -= 0.03f;
        if (currentAlpha <= 0.2f) doAlpha = false;

    }
}