﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Smoke : MonoBehaviour {
    private ParticleSystem ps;

    // Use this for initialization
    void Start ()
    {
       ps = GetComponent<ParticleSystem>();
       ps.Emit(1000);
    }
	
	// Update is called once per frame
	void Update ()
    {
        ps = GetComponent<ParticleSystem>();
        if (!ps.IsAlive()) Destroy(gameObject);

    }
}
