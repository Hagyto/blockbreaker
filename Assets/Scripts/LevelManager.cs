﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour {

    static public float musicLevel = 1, soundLevel = 1f;
    static public bool twoPlayer = false, coopPlayer = false, currentlyP1 = true;

	public void LoadLevel( string name )
	{
        //Only to be called by startbutton!

        twoPlayer = true;

        ScreenManager.P1Score = 0;
        ScreenManager.P2Score = 0;
        ScreenManager.coopScore = 0;
        ScreenManager.P1Balls = 3;
        ScreenManager.P2Balls = 3;
        Ball.hasStarted = false;

        SceneManager.LoadScene( name );
    }

	public void LoadNextLevel()
	{
		Ball.hasStarted = false;
        // gets the curent screen
        Scene sceneLoaded = SceneManager.GetActiveScene();
        // loads next level
        SceneManager.LoadScene(sceneLoaded.buildIndex + 1);
	}

	public void QuitRequest()
	{
		Application.Quit();
	}
}
