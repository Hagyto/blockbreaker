﻿using UnityEngine;
using System.Collections;

public class Ball : MonoBehaviour {

	public static bool hasStarted = false;
    public GameObject smoke;

    private Paddle paddle;
	private Vector3 paddleToBallVector;
	public float vectorSpeed;
    private float[] controlX = new float[3] { 0, 0, 0 };
    private float[] controlY = new float[3] { 0, 0, 0 };
    private int bounceCount = 0;



    // Use this for initialization
    void Start ()
	{

        paddle = GameObject.FindObjectOfType<Paddle>();
		paddleToBallVector = this.transform.position - paddle.transform.position;

		vectorSpeed = 10;
	}
	
	// Update is called once per frame
	void Update () {

		if ( !hasStarted ) 
		{
			this.transform.position = paddle.transform.position + paddleToBallVector;	
			if (Input.GetMouseButtonDown(0))
			{
				hasStarted = true;
				this.GetComponent<Rigidbody2D>().velocity = new Vector2 (2,7);
                Vector2 tweak = new Vector2((this.GetComponent<Rigidbody2D>().position.x - paddle.GetComponent<Rigidbody2D>().position.x) * 7f, 0f);

                tweak = this.GetComponent<Rigidbody2D>().velocity;
                tweak.Normalize();
                tweak = tweak * vectorSpeed;
                this.GetComponent<Rigidbody2D>().velocity = tweak;

                //GameObject smokeRain = Instantiate(smoke, gameObject.transform.position, Quaternion.identity) as GameObject;


            }
        }

	}

	void OnCollisionExit2D (Collision2D collision)
	{
        vectorSpeed += 0.2f;
        if (vectorSpeed > 20) vectorSpeed = 20;

        Vector2 tweak = new Vector2();

        GameObject smokeRain = Instantiate(smoke, gameObject.transform.position, Quaternion.identity) as GameObject;

        if (collision.gameObject.tag != "Paddle")
		{
            ++bounceCount; // count for x/y compare array

            controlX[bounceCount] = this.GetComponent<Rigidbody2D>().position.x;
            controlY[bounceCount] = this.GetComponent<Rigidbody2D>().position.y;

            if (bounceCount >= 2) bounceCount = -1;

            if ( ( (controlX[0] == controlX[1] ) && (controlX[1] == controlX[2]) ) || ( (controlY[0] == controlY[1]) && (controlY[1] == controlY[2]) ) )
            {
                tweak.x = (Random.Range( -10f, 10f));
                tweak.y = (Random.Range( -10f, 10f));
                this.GetComponent<Rigidbody2D>().velocity += tweak;
                //Debug.Log("TWEAKTIME!TWEAKTIME!TWEAKTIME!TWEAKTIME!TWEAKTIME!TWEAKTIME!");
            }
        }
		else
		{
            tweak = new Vector2((this.GetComponent<Rigidbody2D>().position.x - paddle.GetComponent<Rigidbody2D>().position.x) * 7f, 0f);
		}

        this.GetComponent<Rigidbody2D>().velocity += tweak;
        tweak = this.GetComponent<Rigidbody2D>().velocity;
        tweak.Normalize();
        tweak = tweak * vectorSpeed;
        this.GetComponent<Rigidbody2D>().velocity = tweak;

    }

}
