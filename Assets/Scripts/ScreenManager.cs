﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenManager : MonoBehaviour
{

    public bool showMouse = false;
    public static int P1Score = 0, P2Score = 0, HiScore = 0, coopScore = 0, coopHiScore = 0;
    public static int P1Balls = 3, P2Balls = 3;



    // Use this for initialization
    void Start()
    {
        UnityEngine.Cursor.visible = showMouse;
    }

    // Update is called once per frame
    void Update()
    {
    }
}