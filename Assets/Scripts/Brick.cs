﻿using UnityEngine;
using System.Collections;

public class Brick : MonoBehaviour
{

	public int maxHits;
	public bool isDestrucable = true;
	public Sprite[] hitSprite;
    public Sprite shadowSprite, blendSprite;
	public AudioClip boing, broken;
	public GameObject smoke, whiteBrick;
    private ParticleSystem ps;
    public Color smokeColor;

	private LevelManager levelManager;
	private int timesHit;
    private Color whiteAlpha;
    private bool doAlpha;
    private float currentAlpha;
	public static int breakableCount =0;



	//
	// Use this for initialization
	void Start ()
	{
		levelManager = GameObject.FindObjectOfType<LevelManager> ();

        whiteBrick = transform.Find ("whiteBrick").gameObject;  ///GameObject.FindGameObjectWithTag("whiteBrick");
        whiteAlpha.r = 1;
        whiteAlpha.g = 1;
        whiteAlpha.b = 1;
        whiteAlpha.a = 0f;
        doAlpha = false;
        currentAlpha = 1f;
        timesHit = 0;

		if (isDestrucable)	breakableCount++;
	}
	
	// Update is called once per frame
	void Update ()
	{
        if (doAlpha) SetAlpha();
	}

	void OnCollisionExit2D (Collision2D collision)
	{
		AudioSource.PlayClipAtPoint(boing, transform.position);
		if (isDestrucable)
		{
			HandleHit();
		}
        else
        {
            HandleGoldBlock();
        }
	}

    void HandleGoldBlock()
    {
    }


	void HandleHit()
	{
		timesHit = ++timesHit;
		
		if ( maxHits <= timesHit )
		{
			AudioSource.PlayClipAtPoint(broken, transform.position, 10f);

			GameObject smokeRain = Instantiate(smoke, gameObject.transform.position,Quaternion.identity) as GameObject;

#pragma warning disable CS0618 // Typ oder Element ist veraltet
            smokeRain.GetComponent<ParticleSystem>().startColor = smokeColor;
#pragma warning restore CS0618 // Typ oder Element ist veraltet

            DestroyObject(gameObject);
			breakableCount--;
			if (breakableCount <= 0)
			{
				levelManager.LoadNextLevel();
			}
		}
		else /// Load next Sprite
		{
			int spriteIndex = timesHit -1;
			this.GetComponent<SpriteRenderer>().sprite = hitSprite[spriteIndex];
            doAlpha = true;
            currentAlpha = 1f; /// sets currentAlpha on hit or in case brick has been hit another time before faded out.
		}

	}

    void SetAlpha()
    {
        whiteAlpha.a = currentAlpha;
        whiteBrick.GetComponent<SpriteRenderer>().color = whiteAlpha;
        currentAlpha -= 0.03f;
        if (currentAlpha <= 0f) doAlpha = false;

    }
}