﻿using UnityEngine;
using System.Collections;

public class Paddle : MonoBehaviour {
	float MouseXPosition, SetPaddlePosition;

	public bool autoPlay;

	private Ball ball;

	// Use this for initialization
	void Start ()
	{
		ball = GameObject.FindObjectOfType<Ball>();

       // Cursor.lockState = CursorLockMode.Locked;
        //Cursor.lockState = CursorLockMode.None;
		SetPaddlePosition = 8;
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (autoPlay)
		{
			Vector3 paddlePosition = new Vector3(ball.transform.position.x ,this.transform.position.y,this.transform.position.z);
			this.transform.position = paddlePosition;
		}
		else
		{
			PlayerMove();
		}
	}

	void PlayerMove()
	{
        float mouseDelta = Input.GetAxis("Mouse X")/2f;
		
		SetPaddlePosition += mouseDelta; 
		SetPaddlePosition = Mathf.Clamp (SetPaddlePosition, -3f, 19f);
		
		Vector3 paddlePosition = new Vector3(SetPaddlePosition,this.transform.position.y,this.transform.position.z);
		this.transform.position = paddlePosition;
	}

	void OnCollisionExit2D (Collision2D collision)
	{
		if (Ball.hasStarted) 
		{
            AudioSource audio = GetComponent<AudioSource>();
            audio.volume = LevelManager.soundLevel;
            audio.Play();
        }
    }
}
