﻿using UnityEngine;
using System.Collections;

public class BackgroundMusic : MonoBehaviour
{
	static BackgroundMusic instance = null;
	void Awake() {
		if (instance != null) {
			Destroy(gameObject);
		}
		else {
			instance = this;
			GameObject.DontDestroyOnLoad (gameObject);
            AudioSource audio = GetComponent<AudioSource>();
            audio.volume = LevelManager.musicLevel;
        }

    }
}
