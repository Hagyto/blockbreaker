﻿using UnityEngine;
using System.Collections;

public class FallOffScreen : MonoBehaviour {
	private LevelManager levelmanager;

	void Start()
	{
		levelmanager = 	GameObject.FindObjectOfType<LevelManager> ();
	}

	void OnCollisionEnter2D(Collision2D collision)
	{
        if (LevelManager.twoPlayer)
        {
            if (LevelManager.currentlyP1) ScreenManager.P1Balls -= 1;
            if (!LevelManager.currentlyP1) ScreenManager.P2Balls -= 1;

            if (ScreenManager.P1Balls <= 0 & ScreenManager.P2Balls <= 0)
            {
                GameOver();
            }

            if (LevelManager.currentlyP1 & ScreenManager.P2Balls > 0)
            {
                LevelManager.currentlyP1 = false;
            }
            else
            {
                if (ScreenManager.P1Balls > 0) LevelManager.currentlyP1 = true;
            }
            Ball.hasStarted = false;
        }
        else
        {
            ScreenManager.P1Balls -= 1;
            if (ScreenManager.P1Balls <= 0 )
            {
                GameOver();
            }
            else
            {
                Ball.hasStarted = false;
            }
        }

    }

	void OnTriggerEnter2D(Collider2D trigger)
	{
		print ("triggering now");
	}

    void GameOver()
    {
        SimpleBrick.breakableCount = 0;
        levelmanager.LoadLevel("Start");

    }
}
