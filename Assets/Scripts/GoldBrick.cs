﻿using UnityEngine;
using System.Collections;

public class GoldBrick : MonoBehaviour
{
	public GameObject whiteBrick;

    private Color whiteAlpha;
    private bool doAlpha;
    private float currentAlpha;



	//
	// Use this for initialization
	void Start ()
	{
        whiteBrick = transform.Find ("whiteBrick").gameObject;  ///GameObject.FindGameObjectWithTag("whiteBrick");
        whiteAlpha.r = 1;
        whiteAlpha.g = 1;
        whiteAlpha.b = 1;
        whiteAlpha.a = 0f;
        doAlpha = false;
        currentAlpha = 1f;

	}
	
	// Update is called once per frame
	void Update ()
	{
        if (doAlpha) SetAlpha();
	}

	void OnCollisionExit2D (Collision2D collision)
	{
        AudioSource audio = GetComponent<AudioSource>();
        audio.volume = LevelManager.soundLevel;
        audio.Play();

        HandleHit();

	}

    void HandleGoldBlock()
    {
    }


	void HandleHit()
	{
            doAlpha = true;
            currentAlpha = 1f; /// sets currentAlpha on hit or in case brick has been hit another time before faded out.

	}

    void SetAlpha()
    {
        whiteAlpha.a = currentAlpha;
        whiteBrick.GetComponent<SpriteRenderer>().color = whiteAlpha;
        currentAlpha -= 0.03f;
        if (currentAlpha <= 0f) doAlpha = false;

    }
}