﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CanvasUpdate : MonoBehaviour {

    public Text p1Score, p2Score, hiScore;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update() {

        if (ScreenManager.P1Score > ScreenManager.HiScore) ScreenManager.HiScore = ScreenManager.P1Score;
        if (ScreenManager.P2Score > ScreenManager.HiScore) ScreenManager.HiScore = ScreenManager.P2Score;

        if (LevelManager.currentlyP1)  // dimm inactive playerscore
        {
            Color setcol = p1Score.color;
            setcol.a = 1f;
            p1Score.color = setcol;
            setcol = p2Score.color;
            setcol.a = 0.5f;
            p2Score.color = setcol;
        }
        else
        {
            Color setcol = p1Score.color;
            setcol.a = 0.5f;
            p1Score.color = setcol;
            setcol = p2Score.color;
            setcol.a = 1f;
            p2Score.color = setcol;

        }

        WriteScore(ScreenManager.P1Score, ScreenManager.P2Score, ScreenManager.HiScore);
    }

    public void WriteScore(int player1, int player2, int hi)
    {
        string prefixP1 = SetPrefix(player1);
        string prefixP2 = SetPrefix(player2);
        string prefixHi = SetPrefix(hi);

        p1Score.text = "P1: " + prefixP1 + player1;
        p2Score.text = "P2: " + prefixP2 + player2;
        hiScore.text = "HI: " + prefixHi + hi;

    }

    string  SetPrefix(int score)
    {
        string prefix = "";
        if (score < 1000000) prefix = "0";
        if (score < 100000) prefix = "00";
        if (score < 10000) prefix = "000";
        if (score < 1000) prefix = "0000";
        if (score < 100) prefix = "00000";
        if (score < 10) prefix = "000000";

        return prefix;
    }
}
